# Webmin Auto Install Script
Webmin Install Script created by Pablo Sirvent.

Tested on:

  - Ubuntu 15.04/15.10/16.04/16.10

### In the process
You will need to press `yes` when phpmyadmin asks you (I need to fix that)

### Installation
Clone this repository and run `run.sh`

OR

Just download `run.sh` and run it.

If Ubuntu complains about missing dependencies, install them with the command:

`apt-get install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python`
